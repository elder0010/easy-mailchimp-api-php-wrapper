<?php
//Testing..
require_once('Mailchimp_Easywrapper.php');
echo '<pre>';

/*
create a new Mailchimp_Easywrapper using the Mailchimp api key
see http://kb.mailchimp.com/article/where-can-i-find-my-api-key
*/
$key = 'YOUR_API_KEY_HERE';
$mc = new Mailchimp_Easywrapper($key);

#get the list of mailchimp created lists ;)
$lists = $mc->lists();

#uncomment this to see the result
#print_r($lists); 

#get the first list id
$list_id = $lists['data'][0]['id'];	
#print($list_id);

$groups = $mc->list_groupings($list_id);
#print_r($groups);

#create some groups list to assign to the users
$g1 = array(
            'name' => "Users", // You can use either 'name' or 'id' to identify the group
            'groups' => array("Salesman","Manager"),
		 );
		
$groups_1 [] = $g1;

$g2 = 	array(	
			'name' => "Users", // You can use either 'name' or 'id' to identify the group
			'groups' => array("Developer"),
		);
		
$groups_1 [] = $g2;

$g3 = array(
		'name' => "Users", // You can use either 'name' or 'id' to identify the group
		'groups' => array("Tester"),		
);


$groups_2 [] = $g3;
$groups_2 [] = $g2;	
	
#create a couple of users using pack_user
$user_1 = $mc->pack_user('john.doe@8bits.com','John','Doe',$groups_1,'it');
$user_2 = $mc->pack_user('jane.doe@8bits.com','Jane','Doe',$groups_2,'en');

#put the users together..
$users = array();
$users[] = $user_1;
$users[] = $user_2;

var_dump($user_1);
try {
	#Subscribe an user
	$res = $mc->subscribe_one($list_id,$user_1);
	
	#Unsubscribe an user
	#$res = $mc->unsubscribe_one($list_id,$user_1);
	
	#Subscribe many users
	#$res = $mc->subscribe_many($list_id,$users);

	#Unsubscribe many users
	#$res = $mc->unsubscribe_many($list_id,$users);
	
	#uncomment this to see the result
	#print_r($res);
}catch(Exception $e){
	echo $e->getMessage(); #print the error if something bad occurs :(
}
?>