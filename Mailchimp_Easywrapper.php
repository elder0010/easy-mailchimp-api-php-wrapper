<?php 
require('src/Mailchimp.php');

/**
* Mailchimp Easywrapper for PHP
* This class requires (PHP 5 >= 5.2.0)
* Wraps some basic functions of the Mailchimp V2 API
* lists - return the created lists
* subscribe_one - subscribes one user to a list
* subscribe_many - subscribes a list of users to a list
* unsubscribe_one - unsubscribes one user from a list
* unsubscribe_many - unsubscribes a list of users from a list
* each user must be packed using pack_user() function (see below)
* @author     Alessio Cosenza (elder0010)
* @version    0.2  
*/
class Mailchimp_Easywrapper{
	
	private $mc;
	
    function __construct($mailchimp_api_key=false) {
		try{
			$this->mc = new Mailchimp($mailchimp_api_key);
		}catch(Exception $e){
			throw $e;
		}
    }
	
	/**
	* Packs an user following Mailchimp API standars.
	* @param email 
	* @param first_name
	* @param last_name
	* @param groups an array of groupings / groups in the format array('[GROUP_ID]' => 'interestgroup1, interestgroup2, interestgroup3, etc');
	* @param language the code of the language. see http://kb.mailchimp.com/article/can-i-see-what-languages-my-subscribers-use#code for a list
	* @param notes notes on the user
	* @return array with the users data in Mailchimp format
	* @throws Exception if error occurs 
	*/
	public function pack_user($email=false,$first_name=false,$last_name=false,$groups = array(),$language='en',$notes=false){
	
		try{
			
			if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
				throw new Exception('Invalid e-mail');
			}
				
			$data = array();		
			$data['email']['email'] = $email;
			$data['merge_vars']['FNAME'] = $first_name;
			$data['merge_vars']['LNAME'] = $last_name;
			
			
			$data['merge_vars']['GROUPINGS'] = array();
			if(!empty($groups))
			{
				$data['merge_vars']['GROUPINGS'] = $groups;
			}
			
			$data['merge_vars']['MC_LANGUAGE'] = $language;
			
			$data['merge_vars']['MC_NOTES'] = false;
			if(is_string($notes)){
				$data['merge_vars']['MC_NOTES'] = $notes;
			}
		
		}catch(Exception $e){
			throw $e;
		}
		return $data;
	}
	
	
	/**
	* Returns the lists
	*/
	public function lists()
	{
		return $this->mc->call('lists/list',null);
	}
	
	/**
	* Subscribes one packed user on a list.
	* If the user is already present on the list, his data (first name, last name, email will be updated)
	* @param list_id the mailchimp list ID
	* @param $user the user to add (or update) in the list
	* @return array with the user data
	* @throws Exception if error occurs 
	 Array
	(
	    [email] => elder0010@theeightbits.com
	    [euid] => a85100266e
	    [leid] => 4727272
	)
	*/
	public function subscribe_one($list_id=false,$user=false)
	{
		try{
			$mailchimp = $this->mc;
		
			if(!$list_id){
				throw new Execption('Invalid Mailchimp list ID');
			}
			
			foreach($user['merge_vars'] as $k=>$v){
				$$k = $v;
			}
			
			$email = $user['email']['email'];
			
			$res = $mailchimp->call(
							'lists/subscribe',
							array(
					                'id'                => $list_id,
					                'email'             => array('email'=>$email),
					                'merge_vars'        => array(
																	'FNAME'=>$FNAME,
																	'LNAME'=>$LNAME,
																	'GROUPINGS'=>$GROUPINGS,
																	'MC_LANGUAGE'=>$MC_LANGUAGE,
																	'MC_NOTES'=>$MC_NOTES,
																 ),
					                'double_optin'      => false,
					                'update_existing'   => true,
					                'replace_interests' => false,
					                'send_welcome'      => false,
								)
							);
			return $res;
		}catch(Exception $e){
			throw $e;
		}
	
	}

	/**
	* Unsubscribes one packed user on a list.
	* If the user is already present on the list, his data (first name, last name, email will be updated)
	* @param list_id the mailchimp list ID
	* @param $user the user to add (or update) in the list
	* @return array with the unsubscribtions count (1)
	Array
	(
	    [complete] => 1
	)
	* @throws Exception if error occurs (user not present, invalid parameters)
	*/
	public function unsubscribe_one($list_id=false,$user=false)
	{
		try{
			$mailchimp = $this->mc;
		
			if(!$list_id){
				throw new Execption('Invalid Mailchimp list ID');
			}
		
			$fname = $user['merge_vars']['FNAME'];
			$lname = $user['merge_vars']['LNAME'];
			$email = $user['email']['email'];
		
			$res = $mailchimp->call(
							'lists/unsubscribe',
							array(
					                'id'                => $list_id,
					                'email'             => array('email'=>$email),
					                'merge_vars'        => array(
																	'FNAME'=>$fname,
																	'LNAME'=>$lname,
																 ),
				               
					                'send_goodbye' 		=> 	false,
					                'delete_member'      => true,
				                
								)
							);
			return $res;
		}catch(Exception $e){
			throw $e;
		}
	
	}

	/**
	* Subscribes a batch of packed users on a list.
	* If the user is already present on the list, his data (first name, last name, email will be updated)
	* @param list_id the mailchimp list ID
	* @param $users the user array to add (or update) in the list
	* @return array with the users data
	* @throws Exception if error occurs 
	Array
	(
	    [add_count] => 0
	    [adds] => Array
	        (
	        )

	    [update_count] => 2
	    [updates] => Array
	        (
	            [0] => Array
	                (
	                    [email] => mario@iloveatari.com
	                    [euid] => 4432fds2
	                    [leid] => 64481329
	                )

	            [1] => Array
	                (
	                    [email] => luigi@flattextures.com
	                    [euid] => a1166fb047
	                    [leid] => 62380633
	                )

	        )

	    [error_count] => 0
	    [errors] => Array
	        (
	        )

	)
	*/
	public function subscribe_many($list_id=false,$users=false)
	{
		try{
			$mailchimp = $this->mc;
			
			if(empty($users)){
				throw new Exception('Empty users list');
			}
		
			if(!$list_id){
				throw new Execption('Invalid Mailchimp list ID');
			}
		
			$list = array();
		
			foreach($users as $u){
				$list[] = $u;
			}
	
			$res = $mailchimp->call(
							'lists/batch-subscribe',
							array(
					                'id'                => $list_id,
					                'batch'             => $list,
					                'double_optin'      => false,
					                'update_existing'   => true,
					                'replace_interests' => false,
					                'send_welcome'      => false,
								)
							);
			return $res;
		}catch(Exception $e){
			throw $e;
		}	
	}

	/**
	* Unsubscribes a batch of packed users on a list.
	* If the user is already present on the list, his data (first name, last name, email will be updated)
	* @param list_id the mailchimp list ID
	* @param $users the user array to remove from the list
	* @return array with the operation status
	* @throws Exception if error occurs 
	Array
	(
	    [success_count] => 2
	    [error_count] => 0
	    [errors] => Array
	        (
	        )

	)
	*/
	public function unsubscribe_many($list_id=false,$users=false)
	{
		try{
			$mailchimp = $this->mc;
			
			if(empty($users)){
				throw new Exception('Empty users list');
			}
	
			if(!$list_id){
				throw new Execption('Invalid Mailchimp list ID');
			}
		
			$list = array();
		
			foreach($users as $u){
				$u['email'] = $u['email']['email'];
				$list[] = $u;
			}
	
			$res = $mailchimp->call(
							'lists/batch-unsubscribe',
							array(
					                'id'                => $list_id,
					                'batch'             => $list,
					                'send_goodbye' 		=> 	false,
					                'delete_member'      => true,
								)
							);
			return $res;
		}catch(Exception $e){
			throw $e;
		}	
	}
	
	/**
	* Returns the groups of a list
	* @param list_id the mailchimp list ID
	* @param counts count the subscribers of each group (bool)
	* @return array with groups list
	* @throws Exception if error occurs 
	Array
	(
	    [0] => Array
	        (
	            [id] => 2905
	            [name] => Utenti
	            [form_field] => hidden
	            [display_order] => 0
	            [groups] => Array
	                (
	                    [0] => Array
	                        (
	                            [bit] => 1
	                            [name] => Group 1 name
	                            [display_order] => 1
	                            [subscribers] => 1
	                        )

	                    [1] => Array
	                        (
	                            [bit] => 2
	                            [name] => Group 2 name
	                            [display_order] => 2
	                            [subscribers] => 0
	                        )

	                    [2] => Array
	                        (
	                            [bit] => 4
	                            [name] => Group 3 name
	                            [display_order] => 3
	                            [subscribers] => 1
	                        )

	                )

	        )
		)
	*/
	public function list_groupings($list_id,$counts=false)
	{
		try{
			$mailchimp = $this->mc;
			
			return $mailchimp->call('lists/interest-groupings',
							array(
					                'id'             => $list_id,
					                'counts' 		=> 	$counts
								)
							);
		}catch(Exception $e){
			throw $e;
		}	
	}
}
?>