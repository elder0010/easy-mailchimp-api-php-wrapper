Overview
=============================================
A simple PHP wrapper class for [v2 of the MailChimp API](http://apidocs.mailchimp.com/api/2.0/). It wraps some basic API operations.

* lists - return the created lists
* subscribe_one - subscribes one user to a list
* subscribe_many - subscribes a list of users to a list
* unsubscribe_one - unsubscribes one user from a list
* unsubscribe_many - unsubscribes a list of users from a list
* list_groupings - list the groups of a list


###Usage
	require_once('Mailchimp_Easywrapper.php');
	$mc = new Mailchimp_Easywrapper('YOUR_API_KEY_HERE');
	
	//create a couple of users using pack_user 
	$user_1 = $mc->pack_user('john.doe@8bits.com','John','Doe');
	$user_2 = $mc->pack_user('jane.doe@8bits.com','Jane','Doe');

	//put the users together..
	$users = array();
	$users[] = $user_1;
	$users[] = $user_2;

	//Subscribe an user
	$mc->subscribe_one($list_id,$user_1);
	
	//Unsubscribe an user
	$mc->unsubscribe_one($list_id,$user_1);
	
	//Subscribe many users
	$mc->subscribe_many($list_id,$users);

	//Unsubscribe many users
	$mc->unsubscribe_many($list_id,$users);


See the included example.php for a detailed example
---
